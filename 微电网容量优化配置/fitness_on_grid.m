function fitness_value  = fitness_on_grid(x)
   %% fitness_on_grid 并网微电网的适应度函数
    format short
   %% 数据定义
    % 微电网中各种设备数量，依次为风力发电机、光伏电池、蓄电池
    facility_count = num2cell(x);
    
    % 各种费用：总费用 = 年安装建设费用 + 年设备重置费用 + 年运行维护费用 + 购售电费用
    total_cost = 0;
    construct_cost = 0;
    reset_cost = 0;
    maintain_cost = 0;
    purchase_cost = 0;
    sell_cost = 0;
    
    % 各设备各类价格与参数
    single_price = {76000 , 2400 , 1600}; %各设备安装单价 
    reset_price = {68000 , 2200 , 1500}; %各设备重置单价  
    maintain_price = {5.7 , 14.3 , 7}; %各设备维护价格  
    facility_capacity = {10 , 0.3 , 4.8}; %各设备额定容量 
    operate_year_facility = {10 , 10 , 5}; %各设备使用寿命
    
    % 各种其他参数
    r = 0.05; %设备折现率
    Y = {20 , 20 , 5}; %设备使用年限
    operate_year_grid = 60; %微电网预设运行年限
    operate_hours_count = 100; %年运行小时数
    v_in = 2.5; %切入风速
    v_out = 15; %切出风速
    v_rated = 12; %额定风速
    pt = -0.0047; %光伏电池功率温度系数
    temp_rated = 25; %光伏电池参考温度
    radia_rated = 1000; %光伏电池额定光强
    dt = 1; %最小时间间隔为1小时

    % 导入excel表作为输入数据集
    envm_data = readcell('envm_dataset.xlsx'); %环境数据集
    load_data = readcell('load_dataset.xlsx'); %负载数据集

    % 各时刻风力、光伏出力功率
    power_generate = cell(8760 , 2);
    % 发电减负载的功率差值集
    power_differ_set = cell(8760 , 1);
    
    % 蓄电池相关参数变量
    electric_capacity = facility_capacity{3} * facility_count{3}; %蓄电池总额定容量
    electric_quantity = cell(8760 , 1); %蓄电池t时刻电量
    state_of_charge = cell(8760 , 1); %蓄电池t时刻荷电状态SOC
    power_charge_max = 0.2 * electric_capacity; %蓄电池最大充电功率，设为额定功率的20%
    power_discharge_max = 0.2 * electric_capacity;
    efficiency_charge = 0.95; %充电效率
    efficiency_discharge = 0.95;
    soc_max = 0.9;
    soc_min = 0.1;
    soc_initial = soc_min; %蓄电池初始荷电状态
    state_of_charge{1} = soc_initial;
    electric_initial = electric_capacity * soc_initial; %蓄电池初始电量 其值大于等于soc_min时电量 在设置参数时即要满足
    electric_quantity{1} = electric_initial;

    % 购售电量
    electric_sell = cell(8760 , 1); % t时刻售电量
    electric_purchase = cell(8760 , 1); % t时刻购电量
    
    % 分时电价
    purchase_price = {1.21 , 0.69 , 0.43}; %购电时峰平谷三时段对应电价
    sell_price = {1.02 , 0.50 , 0.27}; %售电时峰谷平三时段对应电价
    
    % 测试用变量
    power_charge_discharge_set = cell(8760 , 2);
    electric_charge_discharge_set = cell(8760 , 2);
    
   %% 数据计算_数据无关费用
    % 各设备总装机容量
    facility_sum_capacity = cell(3 , 1);
    for i = 1:numel(facility_count)
        facility_sum_capacity{i} = facility_capacity{i} * facility_count{i};
    end
    
    % 计算年均安装建设费用
    for i = 1:numel(facility_count)
        R1 = r*(1+r)^Y{i} / ((1+r)^Y{i}-1); %折现系数
        construct_cost = construct_cost + facility_count{i} * single_price{i} * R1;
    end
    
    % 计算年均设备重置费用
    for i = 1:numel(facility_count)
        R2 = r / ((1+r)^Y{i}-1); %重置系数
        reset_count = operate_year_grid / operate_year_facility{i} - 1;
        reset_cost = reset_cost + facility_count{i} * reset_price{i} * R2 * reset_count;
    end
    
    % 年运行维护费用
    for i = 1:numel(facility_count)
        maintain_cost = maintain_cost + facility_count{i} * maintain_price{i} * facility_capacity{i};
    end
    

    %% 数据计算_数据相关费用
    for t = 1:operate_hours_count
        % 计算t时刻风力发电机出力功率
        v = envm_data{t , 1}; %当前时刻t风速
        power_wind_rated = facility_count{1} * facility_capacity{1}; %当前微电网总额定风电功率
        if(v<v_in || v>v_out)
            power_wind = 0; %当前时刻t风机出力功率
        elseif(v>v_in && v<v_rated)
            power_wind = power_wind_rated * (v-v_in)/(v_rated-v_in);
        else
            power_wind = power_wind_rated;
        end
        power_generate{t , 1} = power_wind; %将当前时刻t风机出力功率存入元胞数组
        
        % 计算t时刻光伏电池出力功率
        temp = envm_data{t , 2}; %当前时刻t温度
        radia = envm_data{t , 3}; %当前时刻t光照强度
        power_light_rated = facility_count{2} * facility_capacity{2}; %当前微电网总额定光电功率
        power_light = power_light_rated * radia * (1+pt*(temp-temp_rated)) / radia_rated;
        power_generate{t , 2} = power_light; %将当前时刻t光伏电池出力功率存入元胞数组
        
        % 计算功率差值
        power_generate_sum = power_wind + power_light; %分布式电源总发电功率
        power_load = load_data{t , 1}; %负载功率
        power_differ = power_generate_sum - power_load; %功率差值
        power_differ_set{t} = power_differ;

        % 初始设购售电都为0
        electric_sell{t} = 0;
        electric_purchase{t} = 0;
        % 测试用 每时刻充放电功率初设为0
        power_charge_discharge_set{t , 1} = 0;
        power_charge_discharge_set{t , 2} = 0;
        electric_charge_discharge_set{t , 1} = 0;
        electric_charge_discharge_set{t , 2} = 0;

        % 蓄电池充放电与微电网购售电
        if(power_differ > 0)
            % 判断蓄电池是否可充
            if(t == 1)
                soc_present = soc_initial;  %蓄电池当前荷电状态
            else
                soc_present = state_of_charge{t-1}; 
            end
            if(soc_present < 0.9) %可充电
                % 将充电功率限制在最大充电功率以下
                if(power_differ >= power_charge_max)
                    power_charge = power_charge_max; % t时刻实际充电功率
                else
                    power_charge = power_differ;
                end
                power_charge_discharge_set{t , 1} = power_charge;

                % 计算净充电量与可充电量
                electric_charge = power_charge * efficiency_charge * dt; % t时刻净充电量
                electric_charge_discharge_set{t , 1} = electric_charge;
                % t时刻可充电量
                if(t == 1)
                    electric_charge_access = electric_capacity * soc_max - electric_initial; 
                else
                    electric_charge_access = electric_capacity * (soc_max - state_of_charge{t-1}); 
                end

                % 充电与售电
                if(electric_charge > electric_charge_access)
                    % 充满蓄电池
                    electric_quantity{t} = soc_max * electric_capacity;
                    state_of_charge{t} = soc_max;
                    % 出售多余电量
                    electric_sell{t} = electric_charge - electric_charge_access;
                else
                    % 所有电量充给蓄电池
                    if(t == 1)
                        electric_quantity{t} = electric_initial + electric_charge;
                    else
                        electric_quantity{t} = electric_quantity{t-1} + electric_charge;
                    end
                    state_of_charge{t} = electric_quantity{t} / electric_capacity;
                    % 该时刻售电为0
                    electric_sell{t} = 0;
                end
                
            else %蓄电池电量已满，发电差值全部售出
                electric_sell{t} = power_differ;
                % 蓄电池状态保持不变
                if(t == 1)
                    electric_quantity{t} = electric_initial;
                    state_of_charge{t} = soc_initial;
                else
                    electric_quantity{t} = electric_quantity{t-1};
                    state_of_charge{t} = state_of_charge{t-1}; 
                end
            end
        
        %蓄电池放电或并电网购电    
        else 
            % 判断蓄电池存在超过soc_min的电量才可放电
            if(t == 1)
                soc_present = soc_initial;  %蓄电池当前荷电状态
            else
                soc_present = state_of_charge{t-1}; 
            end
            if(soc_present > 0.1)
                % 功率差值取正便于计算
                % power_differ = abs(power_differ);
                if(abs(power_differ) >= power_discharge_max)
                    power_discharge = power_discharge_max; % t时刻实际放电功率
                else
                    power_discharge = abs(power_differ);
                end
                power_charge_discharge_set{t , 2} = power_discharge;

                electric_discharge = power_discharge * efficiency_discharge * dt; % t时刻净放电量
                electric_charge_discharge_set{t , 2} = electric_discharge;
                if(t == 1)
                    electric_discharge_access = electric_initial - electric_capacity * soc_min; 
                else
                    electric_discharge_access = electric_capacity * (state_of_charge{t-1} - soc_min); 
                end

                if(electric_discharge > electric_discharge_access)
                    % 放空蓄电池
                    electric_quantity{t} = soc_min * electric_capacity;
                    state_of_charge{t} = soc_min;
                    % 购买多余电量
                    electric_purchase{t} = electric_discharge - electric_discharge_access;
                else
                    % 蓄电池承担放出所有电量
                    if(t == 1)
                        electric_quantity{t} = electric_initial - electric_discharge;
                    else
                        electric_quantity{t} = electric_quantity{t-1} - electric_discharge;
                    end
                    state_of_charge{t} = electric_quantity{t} / electric_capacity;
                    % 该时刻购电为0
                    electric_purchase{t} = 0;
                end

            else %蓄电池电量不足,全部需求电量通过购买
                electric_purchase{t} = abs(power_differ);
                % 蓄电池状态保持不变
                if(t == 1)
                    electric_quantity{t} = electric_initial;
                    state_of_charge{t} = soc_initial;
                else
                    electric_quantity{t} = electric_quantity{t-1};
                    state_of_charge{t} = state_of_charge{t-1}; 
                end
            end
        end
    end
    
    % 根据分时电价计算购售电费用
    for t = 1:operate_hours_count
        % 售电费用
        % t=1 时为0点，每0~23为一个周期，t对24取余，判断其所属时间段，乘相应电价
        if(mod(t , 24) >= 8 && mod(t , 24) < 12 || mod(t , 24) >= 17 && mod(t , 24) < 21)
            electric_price = sell_price{1};
        elseif(mod(t , 24) >= 12 && mod(t , 24) < 17 || mod(t , 24) >= 21 && mod(t , 24) < 24) 
            electric_price = sell_price{2};
        else
            electric_price = sell_price{3};
        end
        sell_cost = sell_cost + electric_price * electric_sell{t};
        
        % 购电费用
        % t=1 时为0点，每0~23为一个周期，t对24取余，判断其所属时间段，乘相应电价
        if(mod(t , 24) >= 8 && mod(t , 24) < 12 || mod(t , 24) >= 17 && mod(t , 24) < 21)
            electric_price = purchase_price{1};
        elseif(mod(t , 24) >= 12 && mod(t , 24) < 17 || mod(t , 24) >= 21 && mod(t , 24) < 24) 
            electric_price = purchase_price{2};
        else
            electric_price = purchase_price{3};
        end
        purchase_cost = purchase_cost + electric_price * electric_purchase{t};
        
    end
    
    %% 总经济费用
    total_cost = construct_cost + reset_cost + maintain_cost + purchase_cost - sell_cost;
    fitness_value = total_cost;
    
%     disp(['total_cost = ',num2str(total_cost)]);
%     disp(['construct_cost = ',num2str(construct_cost)]);
%     disp(['reset_cost = ',num2str(reset_cost)]);
%     disp(['maintain_cost = ',num2str(maintain_cost)]);
%     disp(['purchase_cost = ',num2str(purchase_cost)]);
%     disp(['sell_cost = ',num2str(sell_cost)]);
    
    % xlswrite('SOC.xlsx', state_of_charge);    

end

